
// 1. import sql library
import java.sql.*;

public class CreateCoffeeDB {

	public static void main(String[] args) {

		// MANDATORY INFORMATION THAT YOU NEED
		// 1. Database name --> coffee
		// 2. Username to login to the database --> root
		// 3. Password ---> ""
		// 4. Server address of your database
		// --> http://localhost

		try {
			// 1. Tell Java WHAT DATABASE you are using!
			// (we are using mysql)
			Class.forName("com.mysql.cj.jdbc.Driver");

			// 2. Connect to the database
			final String DB_URL = "jdbc:mysql://localhost/coffee";
			final String USERNAME = "root";
			final String PASSWORD = "";

			Connection conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			// 3. Create a "Coffee" table
			// --
			// -- 1. DROP IT if it exists
			// -- 2. make it

			System.out.println("Drop any existing table...");

			Statement stmt = conn.createStatement();

			try {
				String sql = "DROP TABLE coffee";
				stmt.execute(sql);
				System.out.println("Coffee table dropped...");
			} catch (Exception e) {
				System.out.println("Don't need to drop anything because:");
				System.out.println("Coffee table does not exist.");
			}

			try {
				// Make a table
				String sql = "CREATE TABLE coffee (" + "Description VARCHAR(255)," + "ProdNum CHAR(10),"
						+ "Price DOUBLE" + ")";

				stmt.execute(sql);
				System.out.println("Created coffee table...");

			} catch (Exception e) {
				System.out.println("ERROR: " + e.getMessage());
			}

			// Refactor code to use BATCH statements
			try {
				System.out.println("Trying to insert data");

				stmt.addBatch("INSERT INTO coffee VALUES ('Steeped Tea2', 14-001, 1.49)");
				stmt.addBatch("INSERT INTO coffee VALUES ('French Vanilla2', 14-002, 1.89)");
				stmt.addBatch("INSERT INTO coffee VALUES ('Brazillian Coffee2', 21-001, 5.00)");
				
				stmt.executeBatch();
				
				System.out.println("Inserted 3 rows...");

			} catch (Exception e) {
				System.out.println("ERROR: " + e.getMessage());
			}
//
//			// update
//			try {
//				System.out.println("Trying to update price of B. coffee");
//
//				String sql = 	"UPDATE coffee " + 
//								"SET price=4.00 " +
//								"WHERE ProdNum='21-001'";
//				
//				stmt.execute(sql);
//				
//			} catch (Exception e) {
//				System.out.println("ERROR: " + e.getMessage());
//			}
//			
			

			// 4. Close the connection to the database
			conn.close();

		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
		}

	}

}
