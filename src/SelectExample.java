import java.sql.*;
import java.util.Scanner;

public class SelectExample {

	public static void main(String[] args) {
		
		// 1. User interface
		System.out.println("Enter a price: ");
		Scanner keyboard = new Scanner(System.in);
		double userPrice = keyboard.nextDouble();
		
		// 2. Connecting to a database
		// ----------------------------
		try {
			// 1. Tell Java WHAT DATABASE you are using!
			// (we are using mysql)
			Class.forName("com.mysql.cj.jdbc.Driver");

			// 2. Connect to the database
			final String DB_URL = "jdbc:mysql://localhost/coffee";
			final String USERNAME = "root";
			final String PASSWORD = "";

			Connection conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			// 3. Write your magical code here
			// REFACTOR code to use a Prepared Statement
			//Statement stmt = conn.createStatement();
			try {
				String sql = "SELECT * FROM coffee WHERE Price <= ?";
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setDouble(1, userPrice);
				
				// Go get the results of the query and 
				// save to the ResultSet object
				ResultSet rs = stmt.executeQuery();
				
				// Loop through your results and do something
				// with the results
				int count = 0;
				while (rs.next()) {
					System.out.println("Row: " + count);
					// 1. Get the value in the description column
					String desc = rs.getString("Description");
					// 2. Get the value in the ProductNum column
					String prodNumber = rs.getString("ProdNum");
					// 3. Get the value in the PRice Column
					double price = rs.getDouble("Price");
					
					System.out.println(desc);
					System.out.println(prodNumber);
					System.out.println(price);
					System.out.println("------");
					count++;
				}

				System.out.println("TOTAL:" + count);
				//System.out.println("Row insert complete");
			} catch(Exception e) {
				System.out.println("ERROR: " + e.getMessage());
			}
			
			
			// 4. Close the connection to the database
			conn.close();

		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
		}

		
	}

}
