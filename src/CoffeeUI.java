import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Scanner;

public class CoffeeUI {

	public static void main(String[] args) {
		// 1. Need the user interface
		// ----------------------------
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter description");
		String description = keyboard.nextLine();
		System.out.println("Enter product number");
		String productNum = keyboard.nextLine();
		System.out.println("Enter price");
		double price = keyboard.nextDouble();
		
		
		// Test that your inputs work
		System.out.println("---------");
		System.out.println(description);
		System.out.println(productNum);
		System.out.println(price);
		
		
		
		
		
		// 2. Template code for connecting to a database
		// ----------------------------
		try {
			// 1. Tell Java WHAT DATABASE you are using!
			// (we are using mysql)
			Class.forName("com.mysql.cj.jdbc.Driver");

			// 2. Connect to the database
			final String DB_URL = "jdbc:mysql://localhost/coffee";
			final String USERNAME = "root";
			final String PASSWORD = "";

			Connection conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			// 3. Write your magical code here
			// REFACTOR code to use a Prepared Statement
			//Statement stmt = conn.createStatement();
			
			try {
				String sql = "INSERT INTO coffee VALUES(?,?,?)";
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setString(1, description);
				stmt.setString(2, productNum);
				stmt.setDouble(3, price);
				
				stmt.executeUpdate();
				
//				String sql = "INSERT INTO coffee VALUES ('" +
//						description + "','" +
//						productNum + "'," + 
//						price + ")";
//				stmt.execute(sql);
				System.out.println("Row insert complete");
			} catch(Exception e) {
				System.out.println("ERROR: " + e.getMessage());
			}
			
			
			// 4. Close the connection to the database
			conn.close();

		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
		}

		
	}
	
	

}
